import React from "react";
import Image from "./components/Image";

function App() {
  return (
    <div className="App">
      <h1>React Image Example</h1>
      <Image src="/image.jpg" width={300} height={400} />
    </div>
  );
}

export default App;
