import React, { useEffect } from "react";
import CSS from "csstype";

// import { thumbnail } from "@cloudinary/url-gen/actions/resize";

/*
    PROPS STRUCTURE
    {
        "width": Number | string(["fill", "fit", "responsive"])
        "height": Number | string(["fill", "fit", "responsive"]) 
        "src": string
        "blur": Boolean
        "lazy": Boolean
        "className": string
        "placeholder": string
        "quality": string
    }
*/

const getBase64StringFromDataURL = (dataURL: string) =>
  dataURL.replace("data:", "").replace(/^.+,/, "");

const Image: React.FC<{
  width: number | string;
  height: number | string;
  src: string;
  blur?: Boolean;
  lazy?: Boolean;
  className?: string;
  placeholder?: string;
  quality?: string;
}> = ({ width, height, src, blur, lazy, className, placeholder, quality }) => {
  const blurStyles: CSS.Properties = {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
  };

  useEffect(() => {
    fetch(src)
      .then((res) => {
        fetch(res.url).then((res) => {
          console.log(res);
        });
      })
      .catch((err) => {
        console.error(err);
      });
  }, [src]);

  return (
    <div
      style={{
        position: "relative",
      }}
    >
      <div style={blurStyles}>
        <img src={src} width={width} height={height} alt="responsive" />
      </div>
    </div>
  );
};

export default Image;
